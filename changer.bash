#!/bin/bash
set -e
RANDOM=$$

set_wallpaper()
{
  # Set picture $1 as desktop background
  # 0 - Auto
  # 1 - Centered
  # 2 - Tiled
  # 3 - Stretched
  # 4 - Scaled
  # 5 - Zoomed

  MONITOR=("HDMI1" "VGA1")
  for m in "${MONITOR[@]}"
  do
    PROPERTY="/backdrop/screen0/monitor${m}/workspace0/"
    xfconf-query -c xfce4-desktop --create -p ${PROPERTY}last-image -s ""
    xfconf-query -c xfce4-desktop -p ${PROPERTY}last-image -s "${WP_PATH}/${m}/${1}"
    xfconf-query -c xfce4-desktop --create -p ${PROPERTY}image-style -s 4
  done
}


HOUR=`date +%H`
#if [ $HOUR -ge 12 -a $HOUR -le 16 ]; then
if [ $[RANDOM % 2] -gt 0 ]; then
    SUBDIR="light"
else
    SUBDIR="dark"
fi

WP_BASE_PATH=$1

WP_PATH="${WP_BASE_PATH}/${SUBDIR}"

files=(`find $WP_PATH/VGA1 -type f -print -regex '*.([jpg|png])'`)

if [ ${#files[@]} -eq 0 ]; then
    exit 1
fi

pic_index=$[RANDOM % ${#files[@]}]
selected_pic=${files[$pic_index]}
selected_pic=$(basename $selected_pic)
set_wallpaper $selected_pic
