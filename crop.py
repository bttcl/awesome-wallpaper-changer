#!/usr/bin/python
import sys
import os
import md5
from PIL import Image, ImageStat


BASEPATH = os.environ.get('HOME') + "/.wallpapers/";

SCREENS = [
    {"name": "VGA1",  "size" : [1024, 1280], "offset" : [120, 0]},
    {"name": "HDMI1", "size" : [1080, 1920], "offset" : [0, 100]}
]


def usage():
    print "Usage:"
    print "%s <image path>" % sys.argv[0]

def main():
    if len(sys.argv) != 3:
        usage()
        exit(1)

    imageName = sys.argv[2]
    BASEPATH = sys.argv[1]
    try:
        baseImage = Image.open(imageName, mode="r");
    except IOError as e:
        print e
        exit(1)




    (b_width, b_height) = baseImage.size

    md5name = md5.new(imageName).hexdigest()
    brightness = ImageStat.Stat(baseImage.convert('L')).rms[0]

    subdir = {True: "dark", False: "light"}[brightness < 127]


    width = height = 0
    for screen in SCREENS:
        height  = max(height, (screen.get("size")[0] + screen.get("offset")[0]))
        width  += screen.get("size")[1] + screen.get("offset")[1]

    k = float(b_width) / float(width)
    k = min(k, float(b_height) / float(height))

    G_W_OFF = int((b_width - width * k) / 2)
    G_H_OFF = int((b_height - height * k) / 2)

    offset = G_W_OFF

    for screen in SCREENS:
        screen_name = screen.get("name")
        (h, w) = screen.get("size")
        (h_off, w_off) = screen.get("offset")


        w = int(w * k)
        h = int(h * k)

        h_off = int(h_off * k)
        w_off = int(w_off * k)


        tmp_image = baseImage.crop((offset + w_off, G_H_OFF + h_off, w + offset + w_off, h + h_off + G_H_OFF))
        tmp_image.save("%s/%s/%s/%s.png" % (BASEPATH, subdir , screen_name, md5.new(imageName).hexdigest()));
        offset += w

if __name__ == '__main__':
    main()